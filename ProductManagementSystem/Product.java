package ProductManagementSystem;

public class Product
{
		String productName;
		int productQuantity;
		double perProductPrice;
		public Product(String productName,int quantity,double perProductPrice)
		{
			this.productName=productName;
			this.productQuantity=quantity;
			this.perProductPrice=perProductPrice;
		}
}
