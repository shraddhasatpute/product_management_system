package ProductManagementSystem;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main
{
	static ArrayList<String>li=new ArrayList();
	public static void loadProductList() throws IOException
	{
		String path="C:\\Users\\sai\\eclipse-workspace\\ProductManagement\\src\\ProductManagementSystem\\Product.txt";
		File file=new File(path);
		FileReader fr=new FileReader(file);
		BufferedReader br=new BufferedReader(fr);
		String line="";
				while((line=br.readLine())!=null)
				{
					li.add(line);
				}
	}
	
	public static void main(String[] args) throws IOException
	{
		int option=0;
		Scanner sc=new Scanner(System.in);
		
		System.out.println("*********Welcome in Product Management System********");
		loadProductList();
		while(option!=5)
		{
			showOption();
			System.out.println("Enter your option: ");
			 option=sc.nextInt();
			if(option==1)
			{
				addProduct();
			}
			
			else
				if(option==2)
				{
					updateProduct();
					
				}else 
					if(option==3)
					{
					searchProduct();
						
					}else
						if(option==4)
						{
							deleteProduct();
						}else
							if(option==5)
							{
								exit();
							}
							else
							{
								System.out.println("You enter wrong choice....");
							}
		}
	}
	public static void exit() throws IOException
	{
		writeArrayListToFile();
		System.out.println(0);
	}

		public static void showOption()
	{
		System.out.println(" 1.Add Product");
		System.out.println(" 2.Update Product");
		System.out.println(" 3.Search Product");
		System.out.println(" 4.Delete Product");
		System.out.println(" 5.Exit");
	}
	
	public static void addProduct() throws IOException
	{
		System.out.println("******Add Product*******");
		Scanner scanner=new Scanner(System.in);
		
		System.out.print("\nEnter Name of Product:");
		String productName=scanner.nextLine();
		
		System.out.print("\nEnter Quantity of Product:");
		int quantity=scanner.nextInt();
		
		System.out.print("\nEnter per Product Price:");
		double price=scanner.nextInt();
		
		li.add(productName+","+quantity+","+price+"\n");
	}
	
	public static void updateProduct() throws IOException
	{
		System.out.println("********Update Product*********");
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter Name of Product you want to Updated :");
		String productNameToUpdate=sc.nextLine();
		
		System.out.println("You are Updating Product: "+productNameToUpdate);
		
	
		System.out.print("\nEnter Name of Product:");
		String productName=sc.nextLine();
		System.out.print("\nEnter Quantity of Product:");
		int quantity=sc.nextInt();
		System.out.print("\nEnter per Product Price:");
		double price=sc.nextInt();
		li.set(getIndexOf(productNameToUpdate),productName+" "+quantity+" "+quantity);
	}
	public static int getIndexOf(String itemName)
	{
		int i=0;
		for(String s:li)
		{
			if(s.contains(itemName))
			{
				return i;
			}
		i=i+1;
		}
		return -1;
	}
	
	
	public static void searchProduct() throws IOException
	{
		System.out.println("*****Search Product*****");
		System.out.println("Enter Product name u want to search");
		Scanner sc=new Scanner(System.in);
		String productNameToSearch=sc.nextLine();
		
		System.out.println("You are Searching the product"+productNameToSearch);
		
		Scanner scanner=new Scanner(System.in);
		System.out.print("\nEnter Name of Product:");
		String productName=scanner.nextLine();
		System.out.print("\nEnter Quantity of Product:");
		int quantity=scanner.nextInt();
		System.out.print("\nEnter per Product Price:");
		double price=scanner.nextInt();
		
		li.indexOf(productNameToSearch);
	}
	public static void deleteProduct() throws IOException
	{
		System.out.println(" Delete Product\n\n");
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Product You Want to Delete :");
		String productNameToDelete=sc.nextLine();
		li.remove(getIndexOf(productNameToDelete));
	}
	public static void writeArrayListToFile() throws IOException
	{
		File file=new File("");
		FileWriter fw=new FileWriter(file);
		BufferedWriter bw=new BufferedWriter(fw);
		
		for(String s: li)
		{
			bw.write(s);
		}
		bw.close();
		fw.close();
	}
	
}
